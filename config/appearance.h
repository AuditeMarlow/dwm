/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 0;        /* 0 means bottom bar */
static const char *fonts[]          = { "Roboto:size=12" };
static const char dmenufont[]       = "Hack:size=13";
static const char col_fg_norm[]     = "#BDAE93";
static const char col_bg_norm[]     = "#282828";
static const char col_bd_norm[]     = "#504945";
static const char col_fg_sel[]      = "#EBDBB2";
static const char col_bg_sel[]      = "#504945";
static const char col_bd_sel[]      = "#B57614";
static const char *colors[][3]      = {
	/*               fg           bg           border   */
	[SchemeNorm] = { col_fg_norm, col_bg_norm, col_bd_norm },
	[SchemeSel]  = { col_fg_sel,  col_bg_sel,  col_bd_sel },
};

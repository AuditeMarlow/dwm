/* rules */
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class            instance            title  tags mask  isfloating  monitor */
	{ "Google-chrome",  NULL,               NULL,  1 << 2,    0,          -1 },
	{ "qutebrowser",    NULL,               NULL,  1 << 2,    0,          -1 },
	{ "Firefox",        NULL,               NULL,  1 << 2,    0,          -1 },
	{ "Spotify",        NULL,               NULL,  1 << 8,    0,          -1 },
	{ "spotify",        NULL,               NULL,  1 << 8,    0,          -1 },
	{ NULL,             "Spotify Premium",  NULL,  1 << 8,    0,          -1 },
	{ "Telegram",       NULL,               NULL,  1 << 9,    0,          -1 },
};
